# TheeJS Setup

 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

To kick-start the setup of a ThreeJS project.

## Requirements

+ [node.js](https://nodejs.org/en/)

## Setup

Run `npm i` to install dependencies

## Run Locally

To run un on local server on port `3000`:

```
npm run start
```

## Build distribution

To build and deploy files to distribution folder `dist`:

```
npm run build
```


## Who do I talk to

+ Lorenzo
