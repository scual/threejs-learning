import * as THREE from 'three';
import BaseVertexShader from './BaseVertexShader'

export default class TextureShader extends BaseVertexShader {
    constructor(texture: THREE.Texture) {
        super();

        this.uniforms = {
            uTexture: { type: "texture", value: texture }
        }
    }

    public getFragment(): string {
        return `
        varying vec2 vUv;
        uniform sampler2D uTexture;

        void main() {
            gl_FragColor = texture2D(uTexture, vUv);
        }
        `;
    }

}