import * as THREE from 'three';
import BaseVertexShader from './BaseVertexShader'


export default class ColorShader extends BaseVertexShader {

    constructor(color: THREE.Vector4 = new THREE.Vector4(1.0, 1.0, 1.0, 1.0)) {
        super();

        this.uniforms = {
            uColor: { type: "vec4", value: color }
        }
    }

    public getFragment() {
        return `
        uniform vec4 uColor;

        void main() {
            gl_FragColor = uColor;
        }`
    }
}
