import ShaderInterface from '../interfaces/ShaderInterface'

export default abstract class BaseVertexShader implements ShaderInterface {
    protected uniforms: any;

    public abstract getFragment(): string;

    public getVertex(): string {
        return `
        varying vec2 vUv;

        void main() {
          vUv = uv;

          vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
          gl_Position = projectionMatrix * modelViewPosition;
        }`
    }

    public getUniforms(): any {
        return this.uniforms;
    }
}