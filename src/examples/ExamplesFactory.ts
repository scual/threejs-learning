import { ExampleType } from "../enums/Enums";
import BaseTemplate from "../templates/BaseTemplate";
import AssetLoaderExample from "./AssetLoaderExample";
import ColorShaderExample from "./ColorShaderExample";
import MultiPassExample from "./MultiPassExample";


export default class ExamplesFactory {

    public static Create(type: ExampleType): BaseTemplate {
        switch (type) {
            case ExampleType.AssetLoader:
                return AssetLoaderExample.Create();
            case ExampleType.ColorShader:
                return ColorShaderExample.Create();
            case ExampleType.MultiPass:
                return MultiPassExample.Create();
        }

    }
}