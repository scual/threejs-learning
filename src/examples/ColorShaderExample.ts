import ShaderTemplate from "../templates/ShaderTemplate";
import ColorShader from "../shaders/ColorShader";
import * as THREE from "three";

export default class ColorShaderExample extends ShaderTemplate {

    public static Create(): ColorShaderExample {
        return new ColorShaderExample();
    }

    protected init(): boolean {
        // load desired shader
        this.shader = new ColorShader(new THREE.Vector4(0.4, 0.56, 0.16, 1.0));
        this.material = new THREE.ShaderMaterial({
            uniforms: this.shader.getUniforms(),
            vertexShader: this.shader.getVertex(),
            fragmentShader: this.shader.getFragment(),
        });

        // initialize super class
        super.init();
        return true;
    }

    protected update(): void {
        this.shader.getUniforms().uColor.value = new THREE.Vector4(0.4, Math.sin(Date.now() / 10000.0), 0.16, 1.0);
    }
}