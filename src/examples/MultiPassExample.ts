import BaseTemplate from "../templates/BaseTemplate";
import * as THREE from "three";

export default class MultiPassExample extends BaseTemplate {

    public static Create(): MultiPassExample {
        return new MultiPassExample();
    }

    private bScene: THREE.Scene;
    private bRenderTarget: THREE.WebGLRenderTarget;
    private bCamera: THREE.Camera;
    private bBox: any;
    private mMesh: any;

    protected init(): boolean {
        this.setupBufferScene();
        this.setupMainScene();

        return true;
    }

    protected update(): void {

    }

    protected render(): void {
        this.renderBufferScene();

        this.mRenderer.setRenderTarget(null);
        super.render();
    }

    private setupMainScene() {
        this.mCamera = new THREE.OrthographicCamera(
            window.innerWidth / -2,
            window.innerWidth / 2,
            window.innerHeight / 2,
            window.innerHeight / -2,
            0.1, 1000,
        )
        const material = new THREE.MeshBasicMaterial({ map: this.bRenderTarget.texture });
        const geometry = new THREE.PlaneGeometry(window.innerWidth / 2, window.innerHeight / 2);
        this.mMesh = new THREE.Mesh(geometry, material);
        this.mMesh.position.z = -1;
        this.mScene.add(this.mMesh);
    }

    private setupBufferScene() {
        this.bCamera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 1000);
        this.bCamera.position.z = 2;
        this.bScene = new THREE.Scene();
        this.bRenderTarget = new THREE.WebGLRenderTarget(
            window.innerWidth,
            window.innerHeight,
            { minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter }
        );

        const light = new THREE.DirectionalLight(0xFFFFFF, 1.0);
        light.position.set(-1, 2, 4);
        this.bScene.add(light);

        const boxGeometry = new THREE.BoxGeometry(1, 1, 1);
        const boxMaterial = new THREE.MeshNormalMaterial();
        this.bBox = new THREE.Mesh(boxGeometry, boxMaterial);
        this.bScene.add(this.bBox);
        this.bScene.background = new THREE.Color(0xFFFFFF)
    }

    private renderBufferScene() {
        this.bBox.rotation.x += 0.01;
        this.bBox.rotation.y += 0.01;

        this.mMesh.rotation.z -= 0.01;
        this.mRenderer.setRenderTarget(this.bRenderTarget);
        this.mRenderer.render(this.bScene, this.bCamera);
    }

}
