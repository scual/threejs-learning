import * as THREE from 'three';
import BaseTemplate from "../templates/BaseTemplate";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import AssetUtils from '../utils/AssetsUtils';


export default class AssetLoaderExample extends BaseTemplate {
    public static Create(): AssetLoaderExample {
        return new AssetLoaderExample();
    }

    private controls: OrbitControls;
    private object: THREE.Object3D;


    protected init(): boolean {
        // set camera
        this.mCamera.position.z = 50;

        // create controls
        this.controls = new OrbitControls(this.mCamera, this.mRenderer.domElement);
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.enableZoom = true;

        // light
        const keyLight = new THREE.DirectionalLight(0xffffff, 0.74);
        keyLight.position.set(-100, 10, 100);
        this.mScene.add(keyLight);

        const fillLight = new THREE.HemisphereLight(0xffffff, 0x444444);
        fillLight.position.set(0, 200, 0);
        this.mScene.add(fillLight);

        const backLight = new THREE.DirectionalLight(0xDAE3E5, 1.0);
        backLight.position.set(100, 0, -100);
        this.mScene.add(backLight);

        // load model
        AssetUtils.LoadObj("sr019_box_design", "./assets/", (obj) => {
            this.object = obj;
            this.object.scale.x *= 2.0;
            this.object.scale.y *= 2.0;
            this.object.scale.z *= 2.0;
            this.mScene.add(this.object);
        })

        return true;
    }

    protected update(): void {
        if (this.object) {
            this.object.rotation.x += 0.001;
            this.object.rotation.y += 0.002;
        }
    }

}