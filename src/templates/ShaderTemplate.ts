import BaseTemplate from "./BaseTemplate";
// import ColorShader from "../shaders/ColorShader"
import * as THREE from 'three';
import BaseVertexShader from "../shaders/BaseVertexShader";


export default class ShaderTemplate extends BaseTemplate {

    public static Create(): ShaderTemplate {
        return new ShaderTemplate();
    }

    protected shader: BaseVertexShader;
    protected material: THREE.Material;

    protected init(): boolean {
        // camera
        this.mCamera = new THREE.OrthographicCamera(
            window.innerWidth / -2,
            window.innerWidth / 2,
            window.innerHeight / 2,
            window.innerHeight / -2,
            0.1, 1000,
        )

        // plane mesh
        const mesh = new THREE.Mesh(new THREE.PlaneGeometry(window.innerWidth, window.innerHeight), this.material);
        mesh.position.z = -1;
        this.mScene.add(mesh);

        return true;
    }

    protected update(): void { }
}