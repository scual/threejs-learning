import * as THREE from 'three'
export default abstract class BaseTemplate {
    protected mCamera: any;
    protected mRenderer: THREE.WebGLRenderer;
    protected mScene: THREE.Scene;

    constructor() {
        this.mScene = new THREE.Scene();

        this.mCamera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 1000);

        this.mRenderer = new THREE.WebGLRenderer({ antialias: true });
        this.mRenderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(this.mRenderer.domElement)

        window.addEventListener('resize', this.onWindowResize);
    }

    protected abstract init(): boolean;
    protected abstract update(): void;

    protected render(): void {
        this.mRenderer.render(this.mScene, this.mCamera);
    };

    protected animate(): void {
        requestAnimationFrame(this.animate.bind(this));

        this.update();
        this.render();
    }

    protected onWindowResize(): void {
        this.mCamera.aspect = window.innerWidth / window.innerHeight;
        this.mCamera.updateProjectionMatrix();

        this.mRenderer.setSize(window.innerWidth, window.innerHeight);
    }

    public run(): boolean {
        if (!this.init()) {
            return false;
        }

        this.animate();
    }

}
