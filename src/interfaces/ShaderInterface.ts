
export default interface ShaderInterface {
    getVertex(): string;
    getFragment(): string;
    getUniforms(): any;
}