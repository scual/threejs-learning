import { OBJLoader2 } from "three/examples/jsm/loaders/OBJLoader2";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader";
import { MtlObjBridge } from "three/examples/jsm/loaders/obj2/bridge/MtlObjBridge.js";
import * as THREE from "three";

export default class AssetUtils {

    public static LoadObj(name: string, path: string, objectLoadedCallback: (object: THREE.Object3D) => void): void {

        const mtlLoader = new MTLLoader();
        mtlLoader.load(
            path + name + '.mtl',
            (mtlResult) => {
                const objLoader = new OBJLoader2();
                objLoader.setModelName(name);
                objLoader.setLogging(false, false);
                objLoader.addMaterials(MtlObjBridge.addMaterialsFromMtlLoader(mtlResult), true);
                objLoader.load(
                    path + name + ".obj",
                    (objResult) => {
                        console.log(`Sucessfully loaded OBJ ${name} from path ${path}`);
                        objectLoadedCallback(objResult);
                    },
                    (objProgress) => {
                        console.log(`Loading OBJ ${name}: ${objProgress.loaded}/${objProgress.total}`);
                    },
                    (objError) => {
                        console.error(`Failed to load OBJ ${name} from path ${path}`, objError);
                    }
                )

            },
            (mtlProgress) => {
                console.log(`Loading MTL ${name}: ${mtlProgress.loaded}/${mtlProgress.total}`);
            },
            (mtlError) => {
                console.error(`Failed to load MTL ${name} from path ${path}`, mtlError);
            });
    }
}