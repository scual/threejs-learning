import ShaderTemplate from "../templates/ShaderTemplate";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import * as THREE from "three";
import AssetUtils from "../utils/AssetsUtils";
import TextureShader from "../shaders/TextureShader";

export default class InteriComprimiApp extends ShaderTemplate {
    public static Create(): InteriComprimiApp {
        return new InteriComprimiApp();
    }

    //#region VARIABLES

    private controls: OrbitControls;

    private buffScene: THREE.Scene;
    private buffTarget: THREE.WebGLRenderTarget;
    private buffCamera: THREE.Camera;
    private buffObj: THREE.Object3D;


    //#endregion - variables

    constructor() {
        super();
    }

    private setupBufferScene() {
        this.buffScene = new THREE.Scene();

        this.buffTarget = new THREE.WebGLRenderTarget(
            window.innerWidth,
            window.innerHeight,
            { minFilter: THREE.LinearMipMapLinearFilter, magFilter: THREE.NearestMipMapNearestFilter }
        );



        this.buffCamera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.01, 1000);
        this.buffCamera.position.z = 50;

        const lightKey = new THREE.AmbientLight(0xffffff, 0.75);
        this.buffScene.add(lightKey);

        AssetUtils.LoadObj("sr019_box_design", "./assets/", (obj) => {
            this.buffObj = obj;
            this.buffObj.scale.x *= 2.0;
            this.buffObj.scale.y *= 2.0;
            this.buffObj.scale.z *= 2.0;
            this.buffScene.add(this.buffObj);
        });

        this.controls = new OrbitControls(this.buffCamera, this.mRenderer.domElement);
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.enableZoom = true;

    }

    protected init(): boolean {

        this.setupBufferScene();

        this.shader = new TextureShader(this.buffTarget.texture);
        this.material = new THREE.ShaderMaterial({
            uniforms: this.shader.getUniforms(),
            vertexShader: this.shader.getVertex(),
            fragmentShader: this.shader.getFragment()
        })

        return super.init();
    }

    protected update(): void {
        if (this.buffObj) {
            this.buffObj.rotation.x += 0.001;
            this.buffObj.rotation.y += 0.002;
        }
    }

    protected render(): void {
        this.mRenderer.setRenderTarget(this.buffTarget);
        this.mRenderer.render(this.buffScene, this.buffCamera);

        this.mRenderer.setRenderTarget(null);
        super.render();
    }
}