import BaseTemplate from "../templates/BaseTemplate";
import { AppType } from "../enums/Enums";
import InteriComprimiApp from "./InteriCoprimiApp";


export default class AppsFactory {

    public static Create(type: AppType): BaseTemplate {
        switch (type) {
            case AppType.InteriCoprimiApp: {
                return InteriComprimiApp.Create();
            }
            default: {
                return InteriComprimiApp.Create();
            }
        }

    }
}