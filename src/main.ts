// import AppsFactory from "./apps/AppsFactory";
// import { AppType } from "./enums/Enums";

// AppsFactory.Create(AppType.InteriCoprimiApp).run();

import ExamplesFactory from "./examples/ExamplesFactory"
import { ExampleType } from "./enums/Enums";

ExamplesFactory.Create(ExampleType.MultiPass).run();
