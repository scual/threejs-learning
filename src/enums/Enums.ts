export enum TemplateType {
    BaseShader = "BaseShader",
}

export enum AppType {
    InteriCoprimiApp = "InteriCoprimiApp",
}

export enum ExampleType {
    AssetLoader = "AssetLoader",
    ColorShader = "ColorShader",
    MultiPass = "MultiPass",
}